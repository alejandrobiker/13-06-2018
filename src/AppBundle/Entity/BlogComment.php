<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogComment
 *
 * @ORM\Table(name="blog_comment", indexes={@ORM\Index(name="blog_comment_post_id_idx", columns={"post_id"})})
 * @ORM\Entity
 */
class BlogComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="autor", type="string", length=20, nullable=false)
     */
    private $autor;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", nullable=false)
     */
    private $contenido;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado_at", type="datetime", nullable=false)
     */
    private $creadoAt;

    /**
     * @var \BlogPost
     *
     * @ORM\ManyToOne(targetEntity="BlogPost")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $post;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set autor
     *
     * @param string $autor
     *
     * @return BlogComment
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get autor
     *
     * @return string
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     *
     * @return BlogComment
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set creadoAt
     *
     * @param \DateTime $creadoAt
     *
     * @return BlogComment
     */
    public function setCreadoAt($creadoAt)
    {
        $this->creadoAt = $creadoAt;

        return $this;
    }

    /**
     * Get creadoAt
     *
     * @return \DateTime
     */
    public function getCreadoAt()
    {
        return $this->creadoAt;
    }

    /**
     * Set post
     *
     * @param \AppBundle\Entity\BlogPost $post
     *
     * @return BlogComment
     */
    public function setPost(\AppBundle\Entity\BlogPost $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AppBundle\Entity\BlogPost
     */
    public function getPost()
    {
        return $this->post;
    }
}
