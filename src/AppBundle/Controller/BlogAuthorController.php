<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogAuthor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Blogpost controller.
 *
 * @Route("autor")
 */

class BlogAuthorController extends Controller
{
    /**
     * @Route("/", name="autor_index")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager(); //Es la conexion y los parametros de la base de datos.
      $autores = $em->getRepository('AppBundle:BlogAuthor')->findAll();//Con el findAll(); buscar todos los datos de la entidad BlogAuthor en BD
      
      return $this->render('blogautor/index.html.twig', array(
            'autores' => $autores,
      ));      
      
    }
  
    /**
     * @Route("/{id}", name="autor_show")
     * @Method("GET")
     */
    public function verAction(BlogAuthor $blogAuthor)
    {
        //$deleteForm = $this->createDeleteForm($blogPost);

        return $this->render('blogautor/show.html.twig', array(
            'autor' => $blogAuthor,
         //   'delete_form' => $deleteForm->createView(),
        ));
      
    }
  
    /** 
     *
     * @Route("/{id}/editar", name="autor_editar")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BlogAuthor $blogAuthor)
    {
        //$deleteForm = $this->createDeleteForm($blogAuthor);
        $editForm = $this->createForm('AppBundle\Form\BlogAuthorType', $blogAuthor);      
        $editForm->handleRequest($request);
        //dump($request);die();
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('autor_show', array('id' => $blogAuthor->getId()));
        }

        return $this->render('blogautor/edit.html.twig', array(
            'autor' => $blogAuthor,
            'edit_form' => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ));
    }
  
  
  
  
  
  
    /**
     * @Route("/familia", name="autor_familia")
     */
    public function familiaAction()
    {
        $em = $this->getDoctrine()->getManager(); //todos los parametros de la base de datos.
        $autores = $em->getRepository('AppBundle:BlogAuthor')->findby(array('lastname'=>'Veloz'));//findby();Busca las palabras con ese dato en especifico
        dump($autores); 
        die(); 
      
    }
    
     /**
     * @Route("/crear", name="autor_crear")
     */
    public function crearAction()
    {
        $em = $this->getDoctrine()->getManager();
      
        $autor = new BlogAuthor(); 
        $autor -> setName("Jose");
        $autor -> setLastname("Ramirez");
        $autor -> setCard(83712);      
        $em -> persist($autor); //el persist = prepara para crear el objeto
      
        $autor1 = new BlogAuthor(); 
        $autor1 -> setName("Ramon");
        $autor1 -> setLastname("Veloz");
        $autor1 -> setCard(9138213);
        $em -> persist($autor1); //el persist = prepara para crear el objeto
      
        $autor2 = new BlogAuthor(); 
        $autor2 -> setName("Omar");
        $autor2 -> setLastname("Gonzalez");
        $autor2 -> setCard(74234823);
        $em -> persist($autor2); //el persist = prepara para crear el objeto
      
        $em -> flush(); //ejecuta la accion anterior, es como un submit(enviar)
        die("Registro guardados con exito."); 
      
    }
  
    /**
     * @Route("/editar/{id}", name="autor_edit")
     */
    public function editarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $autor_editar = $em->getRepository('AppBundle:BlogAuthor')->find($id);
        
        if($autor_editar){
          echo 'vamos por buen camino :)';
          die();
         /* $autor_editar -> setName("Alejandrito"); 
          $autor_editar -> setLastname("Rodriguez"); 
          $autor_editar -> setCard(2019102); 
          $em -> persist($autor_editar);  //preperar    
          $em -> flush(); //ejecutar
          dump($autor_editar); //mostrar por el controlador*/
        }else{
          echo "algo salio mal...";
        }
        die();
    }
  
    /**
     * @Route("/lote/{cantidad}", name="autor_lote")
     */
    public function loteAction($cantidad)
    {
      $em = $this->getDoctrine()->getManager();
      
      for($x=1; $x<=$cantidad; $x++)
      {
          $autor = new BlogAuthor(); 
          $autor -> setName("Autor $x");
          $autor -> setLastname("Apellido $x");
          $autor -> setCard(1000+$x);       
          $em -> persist($autor);  //preparar
          $em -> flush();     //enviar
      }
      
      die("Datos registrados con exito.");
      
    }  
          
    /**
    * @Route("/multiplicar/{numero}", name="autor_multiplicar")
    */
  
    public function multiplicarAction($numero)
    {      
      
      for($n=1; $n<10; $n++){
      $resultado = $n * $numero;  
        
        echo "$n * $numero = " . $resultado ."<br>";
      }
      
      die();
      
    }
  
    /**
    * @Route("/sueldo/{salario}", name="autor_sueldo")
    */
  
    public function sueldoAction($salario)
    {      
       
      if($salario<1000){
          echo "Su sueldo es = $salario y es menor que el sueldo minimo";
      }else{
          echo "Su sueldo es = $salario y sobrepasa el sueldo minimo";
      }
      
      die();
      
    }
  
    /**
    * @Route("/ver/{id}", name="autor_ver")
    */
  
    public function verAutorAction($id)
    {   
      $em = $this->getDoctrine()->getRepository('AppBundle:BlogAuthor');
      $autor = $em->find($id);
      
      return $this->render('blogauthor/ver.html.twig', array(
        "id"=>$autor->getId(),
        "nombre"=>$autor->getName(),
        "apellido"=>$autor->getLastname(),
        "cedula"=>$autor->getCard())
      );
    } 
  
    /**
    * @Route("/todosautor", name="autor_todos")
    */
  
    public function autorIndexAction()
    {   
      $em = $this->getDoctrine()->getRepository('AppBundle:BlogAuthor');
      $autores = $em->findAll();
      
      return $this->render('blogauthor/index.html.twig', array(
         'autores' => $autores,
      ));
    } 
  
  
  
  
  
}
