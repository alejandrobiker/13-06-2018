<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Form\EventType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Event controller.
 *
 * @Route("evento")
 */

class EventController extends Controller
{
    /**
     * @Route("/", name="evento_index")
     */
    public function AllAction()
    {
      $em = $this->getDoctrine()->getManager(); //Es la conexion y los parametros de la base de datos.
      $eventos = $em->getRepository('AppBundle:Event')->findAll();//Con el findAll(); buscar todos los datos de la entidad BlogAuthor en BD
      
      return $this->render('evento/index.html.twig', array(
         'eventos' => $eventos,
      ));
      
    }   

    /**
     * @Route("/nuevo", name="evento_nuevo")
     */
    public function nuevoAction(Request $request)
    {
        $evento = new Event();
        $form = $this->createForm(EventType::class,$evento);
      
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
          $evento = $form->getData();
          
          $em = $this->getDoctrine()->getManager();
          $em -> persist($evento);
          $em -> flush();
          
          return $this->redirectToRoute('evento_index');
        }          
      
        return $this->render('evento/nuevo.html.twig', array("form"=>$form->createView()));
    }
  
    /**
     * @Route("/ver/{id}", name="evento_ver")
     */
    public function verAction($id)
    { 
        $em = $this->getDoctrine()->getManager();
        $evento = $em->getRepository('AppBundle:Event')->find($id);
            
        return $this->render('evento/ver.html.twig', array(
          "id"=>$evento->getId(),
          "nombre"=>$evento->getName(),
          "fecha"=>$evento->getDate(),
          "ciudad"=>$evento->getCity(),
          "poblacion"=>$evento->getPopulation())
        );
      
    }
  
    /**
     * @Route("/editar/{id}", name="evento_editar")
     */
    public function editarAction($id)
    { 
           dump($id);   
           return $this->render('evento/editar.html.twig');
      
//         $em = $this->getDoctrine()->getManager();
//         $evento = $em->getRepository('AppBundle:Event')->find($id);
            
//         return $this->render('evento/ver.html.twig', array(
//           "id"=>$evento->getId(),
//           "nombre"=>$evento->getName(),
//           "fecha"=>$evento->getDate(),
//           "ciudad"=>$evento->getCity(),
//           "poblacion"=>$evento->getPopulation())
//         );
      
    }
    
  
  
  
  
  
  
  
  
  
  
}
